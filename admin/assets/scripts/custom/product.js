import {
  getValues
} from "./product-1.js";


jQuery(document).ready(function () {
  // initiate layout and plugins
  App.init();
  uploadFiles();
  ComponentsDropdowns.init();
  makeList();
  getValues();
  validation();
  var c = document.getElementsByClassName("pdf");
  console.log(c);
  jQuery.post("/getUserName", function (data) {
    //to capatalize the first letter of name
    data = data.replace(/^./, data[0].toUpperCase())
    $("#name").html(data);
    console.log(typeof data);
  });
});

//Pagination starts

//will store the collection of data to be sorted
var list = new Array();

// pageList plays a very important role in the script as it will keep 
// track of the items to display on the current page only.
var pageList = new Array();

// The currentPage variable will keep track of where we are in the pagination
var currentPage = 1;

// numberPerPage dictates the amount of items to show per page.
var numberPerPage = 10;

// numberOfPages is calculated on load and will tell us the total number of pages 
// required to render in the collection. 
// This is necessary if we're going to be doing numeric pagination.
var numberOfPages = 0;

function makeList() {
  // console.log("inside makeList()")
  jQuery.post("/product/retrieveReportDetails").then((data) => data.forEach(element => {
    // console.log(element)
    list.push(element)
  })).then(() => {
    numberOfPages = getNumberOfPages();
    // console.log(numberOfPages)
  }).then(() => {
    // console.log("inside makeList calling loadList")
    loadList()
  })
}

function getNumberOfPages() {
  // console.log(`list.length ${list.length}`)
  return Math.ceil(list.length / numberPerPage);
}

$("#first").on("click", () => {
  currentPage = 1;
  loadList();
})

$("#last").on("click", () => {
  currentPage = numberOfPages;
  loadList();
})

$("#next").on("click", () => {
  currentPage += 1;
  loadList();
})

$("#previous").on("click", () => {
  currentPage -= 1;
  loadList();
})

function loadList() {
  // console.log("inside loadList")
  // splitting our data up into their appropriate page counterparts

  // begin = You start at the current page, minus one, plus the page offset. 
  var begin = ((currentPage - 1) * numberPerPage);

  // end = And the end is just that start number plus the offset again.
  var end = begin + numberPerPage;
  var i = begin;
  // console.log(begin, end)
  setTimeout(() => {
    pageList = [];
    // console.log(list.length)
    if (end > list.length) {
      end = list.length;
    }
    for (var i = begin; i < end; i++) {
      pageList.push(list[i]);
    }
    drawList(); // draws out our data
    check(); // determines the states of the pagination buttons
  }, 1000);
  // pageList = list.slice(begin, end);

}

function drawList() {
  var tableHeader = `<tr>
    <th><input type="checkbox" /></th>
    <th>Title</th>
    <th>Key Sector</th>
    <th>Date</th>
    <th>Publisher</th>
    <th>Price</th>
    <th>Country</th>
    <th>Company</th>
    <th> PDF </th>
    </tr>`
  $("#table").html(tableHeader);
  pageList.forEach((element) => {
    // console.log(element.category);
    var td0 = "<td><input type='checkbox' /></td>";
    var td1 = "<td>" + element.name + "</td>";
    var td2 = "<td>" + element.category + "</td>";
    // setting date format to dd-mm-yyyy
    var date = new Date(element.date);
    var newDate =
      date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
    var td3 = "<td>" + newDate + "</td>";
    var td4 = "<td>" + element.publisher + "</td>";
    var td5 = "<td>" + element.price + "</td>";
    var td6 = "<td>" + element.country + "</td>";
    var td7 = "<td>" + element.companiesCovered + "</td>";
    //pdf
    // var keys = Object.keys(element);
    // var pdf = "No PDF";
    // if (keys.includes("reportPDF"))
    //   pdf = element.reportPDF.name;
    // var td8 = "<td>" + pdf + "</td>";
    var keys = Object.keys(element);

    if (keys.includes("reportPDF")) {
      var pdf = element.reportPDF.name;
      var td8 = "<td>Click Here</td>";
    } else var td8 = "<td>No PDF attached</td>";
    var row =
      "<tr>" + td0 + td1 + td2 + td3 + td4 + td5 + td6 + td7 + td8 + "</tr>";
    // console.log("row = " + row);
    $("#table").append(row);
  });
}

function check() {
  document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
  document.getElementById("previous").disabled = currentPage == 1 ? true : false;
  document.getElementById("first").disabled = currentPage == 1 ? true : false;
  document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
}

//Pagination ends

function uploadFiles() {
  $("#excelInput").prop("disabled", true);
  $("#filesInput").prop("disabled", true);
  $(".option").change(function () {
    // alert(this.value);
    if (this.value == "excel") {
      $("#excelInput").prop("disabled", false);
      $("#filesInput").prop("disabled", true);
      $("#filesInput").val(null);
    }
    if (this.value == "report") {
      $("#filesInput").prop("disabled", false);
      $("#excelInput").prop("disabled", true);
      $("#excelInput").val(null);
    }
  });
}

function validation() {
  //Title
  $("#title").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name) {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Description
  $("#description").keyup(function (event) {
    var input = $(this);
    var message = $(this).val();
    // console.log(message);
    if (message) {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Price
  $("#price").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name) {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Date
  $("#date").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name) {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Publisher
  $("#publisher").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Publisher") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Publisher
  $("#keySector").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Key Sector") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Company
  $("#company").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Company") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Continent
  $("#continent").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Continent") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Region
  $("#region").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Region") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
  //Country
  $("#country").on("input", function () {
    var input = $(this);
    var is_name = input.val();
    if (is_name != "Choose Country") {
      input.removeClass("invalid").addClass("valid");
    } else {
      input.removeClass("valid").addClass("invalid");
    }
  });
}