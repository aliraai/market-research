export function getValues() {
    // console.log("inside get Values");
    //publisher
    jQuery.post("/product/getValues/publisher", insertValues);
    //keySector
    jQuery.post("/product/getValues/keySector", insertValues);
    //Company
    jQuery.post("/product/getValues/company", insertValues);
    //Continent
    jQuery.post("/product/getValues/continent", insertValues);
    //Region
    jQuery.post("/product/getValues/region", insertValues);
    //Country
    jQuery.post("/product/getValues/country", insertValues);
}

function insertValues(data) {
    // console.log(data)
    modal(data);
    filter(data);
}

function modal(data) {
    // console.log("modal");
    //convert object into array
    var result = Object.entries(data[0]);
    //get the key of the object, to use it as an id for the dropdown
    let collection = result[1][0];
    // console.log(collection);
    data.forEach(element => {
        var z = document.createElement("option");
        switch (collection) {
            case "publisher":
                z.setAttribute("value", element.publisher);
                var t = document.createTextNode(element.publisher);
                break;
            case "keySector":
                z.setAttribute("value", element.keySector);
                var t = document.createTextNode(element.keySector);
                break;
            case "company":
                z.setAttribute("value", element.company);
                var t = document.createTextNode(element.company);
                break;
            case "continent":
                z.setAttribute("value", element.continent);
                var t = document.createTextNode(element.continent);
                break;
            case "region":
                z.setAttribute("value", element.region);
                var t = document.createTextNode(element.region);
                break;
            case "country":
                z.setAttribute("value", element.country);
                var t = document.createTextNode(element.country);
                break;
        }
        z.appendChild(t);
        document.getElementById(collection).appendChild(z);
    });
}

function filter(data) {
    //console.log("filter");
    //convert object into array
    var result = Object.entries(data[0]);
    //get the key of the object, to use it as an id for the dropdown
    let collection = result[1][0];
    //console.log(collection);
    data.forEach(element => {
        var z = document.createElement("option");
        switch (collection) {
            case "publisher":
                z.setAttribute("value", element.publisher);
                var t = document.createTextNode(element.publisher);
                break;
            case "keySector":
                z.setAttribute("value", element.keySector);
                var t = document.createTextNode(element.keySector);
                break;
            case "company":
                z.setAttribute("value", element.company);
                var t = document.createTextNode(element.company);
                break;
            case "continent":
                z.setAttribute("value", element.continent);
                var t = document.createTextNode(element.continent);
                break;
            case "region":
                z.setAttribute("value", element.region);
                var t = document.createTextNode(element.region);
                break;
            case "country":
                z.setAttribute("value", element.country);
                var t = document.createTextNode(element.country);
                break;
        }
        z.appendChild(t);
        document.getElementById(collection + "Filter").appendChild(z);
    });
}