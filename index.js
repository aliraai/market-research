const express = require("express");
const fileUpload = require("express-fileupload"); //this middleware is used to get files or anyother form inputs
const session = require("express-session"); //for session


const product = require("./routes/product");


const TWO_HOURS = 1000 * 60 * 60 * 2;

const app = express();
app.use("/product", product)

const {
  PORT = 3000,
    NODE_ENV = "development",

    SESS_NAME = "sid",
    SESS_LIFETIME = TWO_HOURS,
    SESS_SECRET = "ssshhhhh",
} = process.env;

const IN_PROD = NODE_ENV === "production";

//SESSION
app.use(
  session({
    name: SESS_NAME,
    secret: SESS_SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: {
      maxAge: SESS_LIFETIME,
      sameSite: true,
      secure: IN_PROD,
    },
  })
);

//Middleware to check if the user is in session
const redirectLogin = (req, res, next) => {
  if (!req.session.user) {
    res.redirect("/admin");
  } else {
    next();
  }
};


// Routing
app.use("/admin", express.static("admin"));
app.use("/menu", redirectLogin, express.static("admin/menu.html"));
app.use("/admin/product", redirectLogin, express.static("admin/product.html"));

//DATABASE
const db = require("./db");

app.use(fileUpload());

app.post("/getUserName", (req, res) => {
  if (req.session.user) {
    res.send(req.session.user);
  } else {
    res.send("John Doe");
  }
})

app.post("/userLogin", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;
  db.initialize("MR", "users")
    .then((dbCollection) => {
      console.log("inside /userLogin function");
      dbCollection
        .find({
          uname: username,
          password: password,
        })
        .toArray((err, result) => {
          if (err) {
            console.error(err);
            throw err;
          }
          let valid = Object.keys(result).length;
          if (valid > 0) {
            // res.sendFile(__dirname + "/admin/menu.html");
            if (!req.session.user) req.session.user = username;

            res.redirect("/menu");
          } else res.redirect("/admin?result=false");
        });
    })
    .catch((err) => console.error(err));
});

app.get("/userLogout", redirectLogin, (req, res) => {
  req.session.destroy((err) => {
    if (err) console.error(err)
  })
  res.redirect("/admin")
})



app.listen(PORT, () => console.log("http://localhost:3000/admin/"));
// console.log("Running at port 3000");