"use strict";
const express = require("express");
const fileUpload = require("express-fileupload");
const xlsx2json = require("xlsx-json-js");
const mongodb = require("mongodb");
const binary = mongodb.Binary;

let router = express.Router();

const db = require("../db");



router.post("/getValues/:collection", (req, res) => {
    const collection = req.params.collection;
    const values = require("./values");
    values.getValues(collection, function (data) {
        // console.log("index.js")
        // console.log(data);
        res.send(data);
    });
});

router.post("/retrieveReportDetails", (req, res) => {
    db.initialize("MR", "insight")
        .then((dbCollection) => {
            dbCollection.find().toArray((err, result) => {
                if (err) console.error(err);
                //console.log(result)
                res.json(result);
            });
        })
        .catch((err) => console.log("Error accessing database " + err));
});
router.use(fileUpload())

router.post("/upload", function (req, res) {
    const radioOption = req.body.optradio;
    if (radioOption == "report") {
        uploadFiles(req, res);
    } else if (radioOption == "excel") {
        uploadExcel(req, res);
    }
});

function uploadExcel(req, res) {
    // var category = req.body.category;
    // console.log(category);
    var excelFile = req.files.excelFile;
    excelFile
        .mv("./admin/excel/" + excelFile)
        .then(() => {
            var items = new Array();
            const xlsxPath = "./admin/excel/" + excelFile;
            const nativeData = xlsx2json.parse(xlsxPath, {
                type: "binary",
                cellDates: true,
            });
            for (var i = 0; i < nativeData.length; i++) {
                for (var j = 0; j < nativeData[i].data.length; j++) {
                    var item = {
                        name: nativeData[i].data[j][0],
                        description: nativeData[i].data[j][1],
                        date: new Date(nativeData[i].data[j][2]).toLocaleDateString(),
                        publisher: nativeData[i].data[j][3],
                        price: nativeData[i].data[j][4].toString(),
                        category: nativeData[i].data[j][5],
                        reportType: nativeData[i].data[j][6],
                        continent: nativeData[i].data[j][7],
                        region: nativeData[i].data[j][8],
                        country: nativeData[i].data[j][9],
                        noOfPages: nativeData[i].data[j][10].toString(),
                        code: nativeData[i].data[j][11],
                        src: nativeData[i].data[j][12],
                        publishedType: nativeData[i].data[j][13],
                        companiesCovered: nativeData[i].data[j][14],
                    };
                    items.push(item);
                }
            }
            return items;
        })
        .then((items) => {
            const insert = require("./insightDAO");
            // insert.insertFromExcel(items, (a) => {
            //   res.send(a);
            // });
            insert.insertFromExcel(items)
                .then(result => {
                    // var string = "no of values inserted = " + result;
                    // res.send(string)
                    res.redirect("/admin/product")
                })
                .catch(result => res.send(result))
        })
        .catch((err) => console.log(err));
    // res.send();
}

function uploadFiles(req, res) {
    const message = [];
    // 1. check if multiple files are uploaded
    const files = req.files.multipleFiles;
    if (files.length > 0) {
        // 2. if yes create a for each loop
        console.log("Number of files uploaded - ", files.length);
        files.forEach((file) => {
            //2a. for each file, check if it exists in the db
            var fileName = file.name.toString();
            fileName = fileName.split(".")[0];
            console.log(`fileName => ${file.name}`);
            db.initialize("MR", "insight")
                .then(dbCollection => {
                    dbCollection
                        .find({
                            name: fileName,
                        })
                        .toArray(function (err, result) {
                            if (err) throw err;
                            if (result.length > 0) {
                                console.log("YES!");
                                //2b. if it does update that entry by adding that row
                                let file1 = {
                                    name: file.name,
                                    file: binary(file.data),
                                };
                                insertFile(file1, fileName);
                            } else {
                                console.log("NO");
                                message.push(file.name);
                            }
                        });
                })
                .catch(err => console.error(err))
        });
    } else {
        var file = req.files.multipleFiles;
        var fileName = file.name.toString();
        fileName = fileName.split(".")[0];
        console.log(`fileName => ${fileName}`);
        console.log("Only one file is uploaded");
        db.initialize(
                "MR",
                "insight")
            .then(function (dbCollection) {
                console.log("************");
                dbCollection
                    .find({
                        name: fileName,
                    })
                    .toArray(function (err, result) {
                        if (err) throw err;
                        if (result.length > 0) {
                            console.log("YES!");
                            //2b. if it does update that entry by adding that row
                            let file1 = {
                                name: file.name,
                                file: binary(file.data),
                            };
                            insertFile(file1, fileName);
                        } else {
                            console.log("NO");
                            message.push(file.name);
                        }
                    });
            })
            .catch(function (err) {
                throw err;
            });
    }
    res.redirect("/admin/product")
}
//update insight row to include report in DB!
function insertFile(file, fileName) {
    console.log("Inside InsertFile");
    let query = {
        name: fileName,
    };
    let data = {
        $set: {
            reportPDF: file,
        },
    };
    db.initialize("MR", "insight")
        .then(function (dbCollection) {
            dbCollection.updateOne(query, data, (err, collection) => {
                if (err) throw err;
                console.log("Record updated successfully");
                console.log(collection.result);
            });
        })
        .catch(function (err) {
            throw err;
        });
    console.log(file);
}


router.post("/uploadReport", function (req, res) {
    let message = {
        //dbColumnName: formInput
        name: req.body.title,
        description: req.body.description,
        date: dateFormat(req.body.date),
        publisher: req.body.publisher,
        price: req.body.price,
        category: req.body.keySector,
        reportType: req.body.reportType,
        continent: req.body.continent,
        region: req.body.region,
        country: req.body.country,
        publishedType: req.body.publishedType,
        companiesCovered: req.body.company,
        reportPDF: req.files.pdfFile,
    };
    db.initialize(
            "MR",
            "insight")
        .then((dbCollection) => {
            dbCollection.insertOne(message, (err, result) => {
                if (err) throw err;
                // res.send(result);
                // res.sendFile(__dirname + "/admin/product.html");
                res.redirect("/admin/product")
            });
        })
        .catch((err) => {
            throw err;
        });
    // res.json(message);
});

function dateFormat(date) {
    var d = new Date(date);
    var returnDate = d.getMonth() + 1 + "/" + d.getDate() + "/" + d.getFullYear();
    //setting the format to mm-dd-yyyy because when converting from excel to json the default format is mm-dd-yyyy
    //and hence while accepting a single record from the admin, i am setting the date format in similar manner
    console.log(returnDate);
    return returnDate;
}

module.exports = router;