const db = require("../db");

function getValues(collection, successCallback) {
    db.initialize("MR", collection)
        .then(dbCollection => {
            dbCollection.find().toArray((err, result) => {
                if (err) console.error(err);
                successCallback(result);
            })
        })
        .catch(err => console.error(err))
}

module.exports = {
    getValues
};