const db = require('../db')

function insertFromExcel(nativeData) {
    return new Promise((resolve, reject) => {
        console.log("inside insertFromExcel")
        var no = 0;
        db.initialize("MR", "insight")
            .then(dbCollection => {
                dbCollection.insertMany(nativeData, function (err, res) {
                    if (err) throw err;
                    console.log("Number of documents inserted: " + res.insertedCount);
                    no = res.insertedCount
                    resolve(no)
                })
            })
            .catch(err => reject(err))
    })

}

module.exports = {
    insertFromExcel
};