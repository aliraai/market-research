$(function() {
  console.log("ready!");
  description();
  tableOfContent();
  contactUs();
  setInitialCurrencyValues();
  // $(".currency").click(function() {
  //   var variable = $("#nav-inr-tab").text();
  //   // if (variable === "INR")
  //   console.log(variable.trim());
  //   alert("working " + this.value);
  // });
  onCurrencyChange();
  $.CestaFeira();
});

function description() {
  $("#description-title").html("Summary");
  var desc = `
  An unmanned aerial vehicle (UAV), commonly known as a drone, as an unmanned aircraft system (UAS), 
  or by several other names, is an aircraft without a human pilot aboard. The flight of UAVs may operate 
  with various degrees of autonomy: either under remote control by a human operator, or fully or intermittently 
  autonomously, by onboard computers. Compared to manned aircraft, UAVs are often preferred for missions that are too 
  "dull, dirty or dangerous" for humans. They originated mostly in military applications, although their use is 
  expanding in commercial, scientific, recreational, agricultural, and other applications, such as policing and 
  surveillance, aerial photography, agriculture and drone racing. <br>
  The global VTOL UAV market will reach xxx Million USD in 2020 with CAGR xx% 2020-2025. 
  The main contents of the report including:
  <ul>
    <li> Global market size and forecast </li>
    <li> Regional market size, production data and export & import </li>
    <li> Key manufacturers profile, products & services, sales data of business </li>
    <li> Global market size by Major End-Use </li>
  </ul>
  `;
  $("#description").html(desc);
}

function tableOfContent() {
  $("#content-title").html("Table of Content");
  var desc = `
  1 Global Market Overview <br>
  1.1 Scope of Statistics <br>
  1.1.1 Scope of Products <br>
  1.1.2 Scope of Manufacturers <br>
  1.1.3 Scope of End-Use <br>
  1.1.4 Scope of Product Type <br>
  1.1.5 Scope of Regions/Countries <br>
  1.2 Global Market Size <br>
  Fig Global VTOL UAV Market Size and CAGR 2015-2019 (Million USD) <br>
  Fig Global VTOL UAV Market Size and CAGR 2015-2019 (Volume) <br>
  Fig Global VTOL UAV Market Forecast and CAGR 2020-2025 (Million USD) <br>
  Fig Global VTOL UAV Market Forecast and CAGR 2020-2025 (Volume) <br>
  2 Regional Market <br>
  2.1 Regional Sales <br>
  Tab Regional Sales Revenue 2015-2019 (Million USD) <br>
  Tab Regional Sales Volume 2015-2019 (Volume) <br>
  2.2 Regional Demand <br>
  Tab Regional Demand and CAGR List 2015-2019 (Million USD) <br>
  Tab Regional Demand and CAGR List 2015-2019 (Volume) <br>
  Tab Regional Demand Forecast and CAGR 2020-2025 (Million USD) <br>
  Tab Regional Demand Forecast and CAGR 2020-2025 (Volume) <br>
  2.3 Regional Trade <br>
  Tab Regional Export 2015-2019 (Million USD) <br>
  Tab Regional Export 2015-2019 (Volume) <br>
  Tab Regional Import 2015-2019 (Million USD) <br>
  Tab Regional Import 2015-2019 (Volume) <br>
  `;
  $("#table-content").html(desc);
}

function contactUs() {
  $(".contact-title").html("For information:");
  var desc = `
    <a href="contact.html">
     <button class="btn btn-primary" style="float:left;">Contact us </button>
    </a>
    `;
  $(".contact-us").html(desc);
}
class currency {
  constructor(user, users, enterprise) {
    this.user = user;
    this.users = users;
    this.enterprise = enterprise;
  }
}
var inr = new currency("1,06,581.00", "1,42,108.00", "2,13,162.00");
var usd = new currency("1500.00", "2000.00", "3000.00");
var gbp = new currency("1144.77", "1526.36", "2289.54");
var euro = new currency("1324.61", "1766.14", "2649.21");
var yen = new currency("1,64,653.50", "2,19,538.00", "3,29,307.00");
function setInitialCurrencyValues() {
  $(".user").html(inr.user);
  $(".users").html(inr.users);
  $(".enterprise").html(inr.enterprise);
}
function onCurrencyChange() {
  $("#nav-inr-tab").click(function() {
    $(".user").html(inr.user);
    $(".users").html(inr.users);
    $(".enterprise").html(inr.enterprise);
  });
  $("#nav-usd-tab").click(function() {
    $(".user").html(usd.user);
    $(".users").html(usd.users);
    $(".enterprise").html(usd.enterprise);
  });
  $("#nav-gbp-tab").click(function() {
    $(".user").html(gbp.user);
    $(".users").html(gbp.users);
    $(".enterprise").html(gbp.enterprise);
  });
  $("#nav-euro-tab").click(function() {
    $(".user").html(euro.user);
    $(".users").html(euro.users);
    $(".enterprise").html(euro.enterprise);
  });
  $("#nav-yen-tab").click(function() {
    $(".user").html(yen.user);
    $(".users").html(yen.users);
    $(".enterprise").html(yen.enterprise);
  });
}
