export class report {
  constructor(
    name,
    description,
    date,
    publisher,
    price,
    category,
    reportType,
    continent,
    region,
    country,
    noOfPages,
    code,
    src
  ) {
    this.name = name;
    this.description = description;
    this.date = date;
    this.publisher = publisher;
    this.price = price;
    this.category = category;
    this.reportType = reportType;
    this.continent = continent;
    this.region = region;
    this.country = country;
    this.noOfPages = noOfPages;
    this.code = code;
    this.src = src;
  }
}

export function returnReport(name, description, price, date, publisher, src) {
  return `
      <div class="p-2 bd-highlight">
          <div class="card mb-3" style="max-width: 7000px;">
              <div class="row no-gutters">
                  <div class="col-md-2">
                      <img src="${src}" class="card-img" alt="...">
                  </div>
                  <div class="col-md-10">
                      <div class="card-body">
                          <h5 class="card-title">${name}</h5>
                          <p class="card-text">${description}</p>
                          <p class="card-text"><small class="text-muted"><b>$${price}  </b> ${date} by ${publisher} </small></p>
                          
                      </div>
                  </div>
              </div>
          </div>      
      </div>
  `;
}

export function returnInsight(name, description, price, date, publisher, src) {
  return `
  <div class="p-2 bd-highlight">
                <div class="card mb-3" style="max-width: 7000px;">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <img src="images/industries/1.png" class="card-img" alt="..." />
                        </div>
                        <div class="col-md-6">
                            <div class="card-body main-body">
                                <h5 class="card-title">${name}</h5>
                                <p class="card-text">{description}</p>
                                <!-- <p class="card-text"><small class="text-muted"><b>  </b> ${date} by ${publisher} </small></p> -->
                                <div style="margin:1em;"> <button class="btn btn-info" onclick="window.location.href='report.html'"> Read More </button> </div> 
                            </div>
                        </div>
                        <div class="col-md-3 p-3" style="background-color:whitesmoke;">
                          <h5> $${price} </h5>
                          <p> ${date} </p>
                          <p>  ${publisher} </p>
                          <div class=""> <button class="btn btn-warning" onclick="window.location.href='report.html'"> Buy Now</button> </div> 
                        </div> 
                        </div>
                    </div>
                </div>      
            </div>
  `;
}
