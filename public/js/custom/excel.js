//
/* set up XMLHttpRequest */
$(document).ready(function() {
  const jsonfile = require("jsonfile");
  const file = "json/category.json";
  jsonfile.readFile(file, function(err, obj) {
    if (err) console.error(err);
    console.dir(obj);
  });
  alert();
});
function ExcelToJSON() {
  var url = "industry.xlsx";
  var oReq = new XMLHttpRequest();
  oReq.open("GET", url, true);
  oReq.responseType = "arraybuffer";
  oReq.onload = function(e) {
    var arraybuffer = oReq.response;
    /* convert data to binary string */
    var data = new Uint8Array(arraybuffer);
    var arr = new Array();
    for (var i = 0; i != data.length; ++i)
      arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join("");
    /* Call XLSX */
    var workbook = XLSX.read(bstr, { type: "binary" });
    /* DO SOMETHING WITH workbook HERE */
    var first_sheet_name = workbook.SheetNames[1];
    /* Get worksheet */
    var worksheet = workbook.Sheets[first_sheet_name];
    var json = XLSX.utils.sheet_to_json(worksheet, "out.json");
    console.log(json);
  };
  oReq.send();
}
