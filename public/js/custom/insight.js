import { returnInsight } from "./reportClass.js";
import {} from "./jquery.twbsPagination.min.js";

$(document).ready(function() {
  getReport();
  insertKeySectors();
  insertCompany();
  insertKeyRegions();
  onRadioButtonChange();
  // alert(monthDiff("28 Feburary, 2019"));
  $("#reset").on("click", function() {
    //alert("Reset");
    reset();
    getReport();
  });
  // $("#pagination-demo").twbsPagination({
  //   totalPages: 16,
  //   visiblePages: 6,
  //   next: "Next",
  //   prev: "Prev",
  //   onPageClick: function(event, page) {
  //     //fetch content and render here
  //     // $("#page-content").text("Page " + page) + " content here";
  //     getReport();
  //   }
  // });
});

function insertKeySectors() {
  const url = "json/category.json";

  var myDiv = document.getElementById("keySectors");

  $.getJSON(url, function(data) {
    var i = 1;
    var label = document.createElement("label");
    label.classList.add("form-check");

    var checkbox = document.createElement("input");
    checkbox.checked = true;
    checkbox.classList.add("form-check-input", "key");
    checkbox.setAttribute("type", "checkbox");
    checkbox.setAttribute("name", "keySector");
    checkbox.setAttribute("value", "All");
    checkbox.setAttribute("id", "keySector_" + i++);
    checkbox.addEventListener("change", onChange);
    label.appendChild(checkbox);

    var span = document.createElement("span");
    span.classList.add("form-check-label");
    var text = document.createTextNode("All");
    span.appendChild(text);

    label.appendChild(span);

    myDiv.appendChild(label);
    data.forEach(element => {
      var label = document.createElement("label");
      label.classList.add("form-check");

      var checkbox = document.createElement("input");
      checkbox.classList.add("form-check-input", "key");
      checkbox.setAttribute("type", "checkbox");
      checkbox.setAttribute("name", "keySector");
      checkbox.setAttribute("value", element.category);
      checkbox.setAttribute("id", "keySector_" + i++);
      checkbox.addEventListener("change", onChange);
      label.appendChild(checkbox);

      var span = document.createElement("span");
      span.classList.add("form-check-label");
      var text = document.createTextNode(element.category);
      span.appendChild(text);

      label.appendChild(span);

      myDiv.appendChild(label);
    });
  });
}

function insertCompany() {
  const url = "json/companiesCovered.json";

  var myDiv = document.getElementById("company");

  $.getJSON(url, function(data) {
    var i = 1;
    data.forEach(element => {
      var label = document.createElement("label");
      label.classList.add("form-check");

      var checkbox = document.createElement("input");
      if (i == 1) {
        checkbox.checked = true;
      }
      checkbox.setAttribute("type", "checkbox");
      checkbox.setAttribute("name", "companiesCovered");
      checkbox.setAttribute("value", element.companiesCovered);
      checkbox.setAttribute("id", "companiesCovered_" + i++);

      checkbox.classList.add("form-check-input");
      checkbox.addEventListener("change", onChange);

      label.appendChild(checkbox);

      var span = document.createElement("span");
      span.classList.add("form-check-label");
      var text = document.createTextNode(element.companiesCovered);
      span.appendChild(text);

      label.appendChild(span);

      myDiv.appendChild(label);
    });
  });
}

function insertKeyRegions() {
  const url = "json/continents.json";

  var myDiv = document.getElementById("keyRegions");

  $.getJSON(url, function(data) {
    var i = 1;
    data.forEach(element => {
      var label = document.createElement("label");
      label.classList.add("form-check");

      var checkbox = document.createElement("input");
      if (i == 1) {
        checkbox.checked = true;
      }
      checkbox.setAttribute("type", "checkbox");
      checkbox.setAttribute("name", "keyRegions");
      checkbox.setAttribute("value", element.continents);
      checkbox.setAttribute("id", "keyRegionsCovered_" + i++);
      checkbox.classList.add("form-check-input");
      checkbox.addEventListener("change", onChange);
      label.appendChild(checkbox);

      var span = document.createElement("span");
      span.classList.add("form-check-label");
      var text = document.createTextNode(element.continents);
      span.appendChild(text);

      label.appendChild(span);

      myDiv.appendChild(label);
    });
  });
}

function getReport() {
  $(".all").prop("checked", true);
  // document.getElementById("keyRegionsCovered_1").checked = true;
  const url = "json/Insight.json";
  // $.getJSON(url, function(data) {
  //   // console.log(data[ 0]);
  //   // alert(data.length);
  //   var desc = "";
  //   data.forEach(element => {
  //     desc += returnInsight(
  //       element.name,
  //       element.description,
  //       element.price,
  //       element.date,
  //       element.publisher,
  //       element.src
  //     );
  //   });
  //   // console.log(desc);
  //   $(".report").html(desc);
  // });
  jQuery.post("/retrieveReportDetails", function(data) {
    // console.log(data);
    // console.log(typeof data);
    // console.log(data.keys());
    // console.log(data[0]);
    //   // alert(data.length);
    var desc = "";
    data.forEach(element => {
      desc += returnInsight(
        element.name,
        element.description,
        element.price,
        element.date,
        element.publisher,
        element.src
      );
    });
    // console.log(desc);
    $(".report").html(desc);
  });
}

function onChange() {
  // alert(this.name);
  switch (this.name) {
    case "keySector":
      check();
      // alert(this.value);
      if (this.value != "All") {
        // alert(this.name);
        insertBadges(this.value, this.name);
      }
      break;
    case "companiesCovered":
      check();
      if (this.value != "All") {
        // alert(this.name);
        insertBadges(this.value, this.name);
      }
      // alert(this.value);
      break;
    case "keyRegions":
      check();
      if (this.value != "Global") {
        // alert(this.name);
        insertBadges(this.value, this.name);
      }
      // alert(this.value);
      break;
  }
}

function onRadioButtonChange() {
  $(".publishedType").on("change", function() {
    // alert(this.value);
    check();
    insertBadge("div_publishedType", this.value);
  });
  $(".date").on("change", function() {
    // alert(this.value);
    check();
    var value;
    switch (+this.value) {
      case 1: //less than one month
        value = "< 1 month";
        break;
      case 2: //less than three months
        value = "< 3 months";
        break;
      case 3: //less than six months
        value = "< 6 months";
        break;
      case 4: //less than 1 year
        value = "< 1 year";
        break;
      case 5: //less than 2 years
        value = "< 2 years";
        break;
      default:
        alert(this.value);
    }
    insertBadge("div_date", value);
  });
}

function check() {
  const url = "json/Insight.json";
  $.getJSON(url, function(data) {
    var data1 = checkDate(data);
    data1 = checkPublishedType(data1);
    data1 = checkKeyRegions(data1);
    data1 = checkCompaniesCoverd(data1);
    data1 = checkKeySector(data1);

    console.log("data1", data1);
    var desc = "";
    data1.forEach(element => {
      desc += returnInsight(
        element.name,
        element.description,
        element.price,
        element.date,
        element.publisher,
        element.src
      );
    });
    var noReport = "No reports found based on the given criteria";
    desc === "" ? $(".report").html(noReport) : $(".report").html(desc);
  });
}

function checkKeySector(data) {
  var keySectors = [];
  $.each($("input[name='keySector']:checked"), function() {
    keySectors.push($(this).val());
  });

  var info = [];
  var i = 0;
  data.forEach(element => {
    keySectors.forEach(ele => {
      var isKeySector = ele === "All" ? true : filter(ele, element.category);
      if (isKeySector) {
        console.log("inside if");
        info[i++] = element;
      }
    });
  });
  return info;
}

function checkCompaniesCoverd(data) {
  var companiesCovered = [];
  $.each($("input[name='companiesCovered']:checked"), function() {
    companiesCovered.push($(this).val());
  });
  //alert(companiesCovered);
  var info = [];
  var i = 0;
  data.forEach(element => {
    companiesCovered.forEach(ele => {
      var isCompaniesCovered =
        ele === "All" ? true : filter(ele, element.companiesCovered);
      if (isCompaniesCovered) {
        console.log("inside if");
        info[i++] = element;
      }
    });
  });
  return info;
}

function checkKeyRegions(data) {
  var keyRegions = [];
  $.each($("input[name='keyRegions']:checked"), function() {
    keyRegions.push($(this).val());
  });
  console.log("keyRegions", keyRegions);
  var info = [];
  var i = 0;
  data.forEach(element => {
    keyRegions.forEach(ele => {
      var isKeyRegions =
        ele === "Global" ? true : filter(ele, element.keyRegion);
      if (isKeyRegions) {
        console.log("inside if");
        info[i++] = element;
      }
    });
  });
  return info;
}
function checkPublishedType(data) {
  var publishedType = $("input[name='publishedType']:checked").val();
  console.log("publishedType", publishedType);
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isPublishedType =
      publishedType === "all"
        ? true
        : filter(publishedType, element.publishedType);
    if (isPublishedType) {
      console.log("Published Type", publishedType);
      info[i++] = element;
    }
  });
  return info;
}
function checkDate(data) {
  var date = $("input[name='periodByMonth']:checked").val();
  console.log("DATE", date);
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isDate = date === "all" ? true : filterDate(date, element.date);
    if (isDate) {
      // alert(isDate);
      console.log("DATE", element.date);
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function filterDate(a, b) {
  var diff = monthDiff(b);
  switch (+a) {
    case 1: //less than one month
      if (diff < 1) {
        return true;
      } else return false;
      break;
    case 2: //less than 3 month
      if (diff < 3) {
        return true;
      } else return false;
      break;
    case 3: //less than 6 month
      if (diff < 6) {
        return true;
      } else return false;
      break;
    case 4: //less than 12 month
      if (diff < 12) {
        return true;
      } else return false;
      break;
    case 5: //less than 24 month
      if (diff < 24) {
        return true;
      } else return false;
      break;
  }
}

function monthDiff(dt1) {
  dt1 = new Date(dt1);
  var dt2 = new Date();
  var diffMonth = (dt2.getTime() - dt1.getTime()) / 1000;
  diffMonth /= 60 * 60 * 24 * 7 * 4;
  return Math.abs(Math.round(diffMonth));
}
function filter(a, b) {
  if (a == b) {
    console.log("in filter", a, b);
    return true;
  } else {
    return false;
  }
}

function reset() {
  insertKeySectors();
  insertCompany();
  insertKeyRegions();
  $("#div_publishedType_close").remove();
  $("#div_date_close").remove();
  var total_element = $(".element").length;
  for (var i = 2; i <= total_element; i++) {
    $("#remove_" + i).remove();
  }
}

function insertBadges(val, nm) {
  var total_element = $(".element").length;
  // alert(nm);
  $.each($("input[name=" + nm + "]:checked"), function() {
    var val1 = $(this).val();
    if (val == val1) {
      var lastid = $(".element:last").attr("id");
      var split_id = lastid.split("_");
      var nextindex = Number(split_id[1]) + 1;
      $(".element:last").after(
        "<div class='element' id='div_" + nextindex + "'></div>"
      );
      var span = $("<span>", {
        id: "remove_" + nm + "_" + nextindex,
        val: val
      }).click(close_continent);
      span.addClass(["badge", "badge-pill", "badge-secondary", "p-2", "m-1"]);
      var icon = $("<i>");
      icon.addClass(["fa", "fa-close", "ml-1"]);
      span.text(val);
      span.append(icon);
      // div = "#" + div;
      $("#div_" + nextindex).append(span);
    }
  });
}
function close_continent() {
  // alert(this.id);
  var id = this.id.split("_");
  // alert(id[1]);
  var a = $("#" + this.id).text();
  // alert(a);
  var list = [];
  $.each($("input[name=" + id[1] + "]:checked"), function() {
    list.push($(this).val());
    var b = $(this).val();
    // alert(b);
    // alert(`working ${a}`);
    if (a == b) $(this).prop("checked", false);
  });
  $("#" + this.id).remove();
  alert(list.length);
  if (list.length == 1) {
    $.each($("input[name=" + id[1] + "]"), function() {
      if ($(this).val() == "All" || $(this.val() == "Global")) {
        // alert("...");
        $(this).prop("checked", true);
      }
    });
  }
  check();
}

function insertBadge(div, val) {
  var div_close = div + "_close";
  var span = $("<span>", {
    id: div_close
  }).click(close);
  span.addClass(["badge", "badge-pill", "badge-secondary", "p-2", "m-1"]);
  var icon = $("<i>");
  icon.addClass(["fa", "fa-close", "ml-1"]);
  span.text(val);
  span.append(icon);
  div = "#" + div;
  $(div).html(span);
}
function close() {
  // alert(this.id);
  var id = this.id.split("_");
  var radio_id = "#" + id[1] + "-all";
  alert(id[1]);
  // alert(id[1]);
  switch (id[1]) {
    case "publishedType":
    case "date":
      $(radio_id).prop("checked", true);
      break;
  }
  check();
  alert("#" + this.id);
  $("#" + this.id).remove();
}
