import { report, returnReport } from "./reportClass.js";

$(document).ready(function() {
  // displayReports();
  insertCountry();
  insertRegion();
  insertPublisher();
  insertCategory();
  getReport();
  OnChange();
  $("#reset").on("click", function() {
    // alert("Reset");
    reset();
    getReport();
  });
});

function getReport() {
  $(".all").prop("checked", true);
  $("#global").prop("checked", true);
  const url = "json/aerospace.json";
  $.getJSON(url, function(data) {
    // console.log(data[ 0]);
    var desc = "";
    data.forEach(element => {
      desc += returnReport(
        element.name,
        element.description,
        element.price,
        element.date,
        element.publisher,
        element.src
      );
    });
    // console.log(desc);
    $(".report").html(desc);
  });
}

function insertCountry() {
  var x = document.createElement("SELECT");
  x.setAttribute("id", "country");
  x.classList.add("form-control");
  document.getElementById("country-dropdown").appendChild(x);
  const url = "json/country.json";
  var z = document.createElement("option");
  z.selected = true;
  z.disabled = true;
  var t = document.createTextNode("Choose Country");
  z.appendChild(t);
  document.getElementById("country").appendChild(z);
  // Populate dropdown with list of provinces
  $.getJSON(url, function(data) {
    // console.log(data);
    data.forEach(element => {
      z = document.createElement("option");
      z.setAttribute("value", element.country);
      var t = document.createTextNode(element.country);
      z.appendChild(t);
      document.getElementById("country").appendChild(z);
    });
  });
}

function insertRegion() {
  let dropdown = $("#region-dropdown");
  var x = document.createElement("SELECT");
  x.setAttribute("id", "region");
  x.classList.add("form-control");
  document.getElementById("region-dropdown").appendChild(x);
  const url = "json/region.json";

  var z = document.createElement("option");
  // z.setAttribute("value", "volvocar");
  z.selected = true;
  z.disabled = true;
  var t = document.createTextNode("Choose Region");
  z.appendChild(t);
  document.getElementById("region").appendChild(z);

  // Populate dropdown with list of provinces
  $.getJSON(url, function(data) {
    // console.log(data);
    var i = 0;
    data.forEach(element => {
      //console.log(element.region + " " + ++i);
      z = document.createElement("option");
      z.setAttribute("value", element.region);
      var t = document.createTextNode(element.region);
      z.appendChild(t);
      document.getElementById("region").appendChild(z);
    });
  });
}

function insertPublisher() {
  var x = document.createElement("SELECT");
  x.setAttribute("id", "publisher");
  x.classList.add("form-control");
  document.getElementById("publisher-dropdown").appendChild(x);
  const url = "json/publisher.json";

  var z = document.createElement("option");
  // z.setAttribute("value", "volvocar");
  z.selected = true;
  z.disabled = true;
  var t = document.createTextNode("Choose Publisher");
  z.appendChild(t);
  document.getElementById("publisher").appendChild(z);

  // Populate dropdown with list of publishers
  $.getJSON(url, function(data) {
    // console.log(data);
    data.forEach(element => {
      z = document.createElement("option");
      z.setAttribute("value", element.publisher);
      var t = document.createTextNode(element.publisher);
      z.appendChild(t);
      document.getElementById("publisher").appendChild(z);
    });
  });
}

function insertCategory() {
  var x = document.createElement("SELECT");
  x.setAttribute("id", "category");
  x.classList.add("form-control");
  document.getElementById("category-dropdown").appendChild(x);

  var z = document.createElement("option");
  // z.setAttribute("value", "volvocar");
  z.setAttribute("id", "category-all");
  z.selected = true;
  z.disabled = true;
  var t = document.createTextNode("Choose Category");
  z.appendChild(t);
  document.getElementById("category").appendChild(z);

  const url = "json/category.json";
  $.getJSON(url, function(data) {
    data.forEach(element => {
      z = document.createElement("option");
      z.setAttribute("value", element.category);
      var t = document.createTextNode(element.category);
      z.appendChild(t);
      document.getElementById("category").appendChild(z);
    });
  });
}

//price, date, category, publisher, report, continents, region, country
function OnChange() {
  // check();
  $(".price").on("change", function() {
    check();
    var text = "";
    switch (+this.value) {
      case 1:
        text = "0-500";
        break;
      case 2:
        text = "501-1000";
        break;
      case 3:
        text = "1001-2000";
        break;
      case 4:
        text = "2001-3000";
        break;
      case 5:
        text = "3000+";
        break;
      default: // all remove the badge
    }
    insertBadge("div_price", text);
  });

  $(".date").on("change", function() {
    check();
    // insertBadges(this.value);
    var text = this.value;
    var text = "";
    switch (+this.value) {
      case 1:
        text = "< one month";
        break;
      case 3:
        text = "< three months";
        break;
      case 6:
        text = "< six months";
        break;
      case 2:
        text = "< 1 year";
        break;
      default: // all remove the badge
    }
    text != "" ? insertBadge("div_date", text) : "";
  });

  $("#category").on("change", function() {
    check();
    insertBadge("div_category", this.value);
  });

  $("#publisher").on("change", function() {
    check();
    insertBadge("div_publisher", this.value);
  });
  $(".report").on("change", function() {
    check();
    // insertBadges(this.value);
    insertBadge("div_report", this.value);
  });

  $(".continent").on("change", function() {
    check();
    if (this.value != "all") insertBadges(this.value);
  });

  $("#region").on("change", function() {
    check();
    // insertBadges(this.value);
    insertBadge("div_region", this.value);
  });

  $("#country").on("change", function() {
    check();
    // insertBadges(this.value);
    insertBadge("div_country", this.value);
  });
}

function check() {
  const url = "json/aerospace.json";
  $.getJSON(url, function(data) {
    var data1 = checkPriceFunction(data);
    data1 = checkDate(data1);
    data1 = checkCategory(data1);
    data1 = checkPublisher(data1);
    data1 = checkReport(data1);
    data1 = checkRegion(data1);
    data1 = checkCountry(data1);
    data1 = checkContinent(data1);
    console.log("data1", data1);
    var desc = "";
    data1.forEach(element => {
      desc += returnReport(
        element.name,
        element.description,
        element.price,
        element.date,
        element.publisher,
        element.src
      );
    });
    var noReport = "No reports found based on the given criteria";
    desc === "" ? $(".report").html(noReport) : $(".report").html(desc);
  });
}
function checkPriceFunction(data) {
  var price = $("input[name='price']:checked").val();
  console.log("PRICE ", price);
  var isPrice = true;
  var info = [];
  var i = 0;
  data.forEach(element => {
    isPrice = checkPrice(price, element.price);
    if (isPrice) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkPrice(a, b) {
  // console.log(a, b);
  switch (+a) {
    case 0:
      // displayReports();
      return true;
      break;
    case 1:
      return filterPrice(0, 500, b);
    case 2:
      return filterPrice(501, 1000, b);
    case 3:
      return filterPrice(1001, 2000, b);
    case 4:
      return filterPrice(2001, 3000, b);
    case 5:
      return filterPrice(3000, null, b);
  }
}
function filterPrice(a, b, cost) {
  var desc = "";
  //console.log(typeof b);
  if (b == null) {
    if (cost >= a) {
      return true;
    }
  } else {
    if (cost >= a && cost <= b) {
      return true;
    }
  }
  return false;
}
function filter(a, b) {
  if (a == b) {
    // console.log(a, b);
    return true;
  } else {
    return false;
  }
}
function checkCategory(data) {
  var category = $("#category")
    .children(":selected")
    .text();
  console.log("CATEGORY", category);
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isCategory =
      category === "Choose Category"
        ? true
        : filter(category, element.category);
    if (isCategory) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkPublisher(data) {
  var publisher = $("#publisher")
    .children(":selected")
    .text();
  console.log("PUBLISHER", publisher);
  // var isCategory = true;
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isPublisher =
      publisher === "Choose Publisher"
        ? true
        : filter(publisher, element.publisher);
    if (isPublisher) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkReport(data) {
  var report = $("input[name='report']:checked").val();
  console.log("REPORT", report);
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isReport = report === "all" ? true : filter(report, element.reportType);
    if (isReport) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkRegion(data) {
  var region = $("#region")
    .children(":selected")
    .text();
  console.log("REGION", region);
  // var isCategory = true;
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isRegion =
      region === "Choose Region" ? true : filter(region, element.region);
    if (isRegion) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkCountry(data) {
  var country = $("#country")
    .children(":selected")
    .text();
  console.log("COUNTRY", country);
  // var isCategory = true;
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isCountry =
      country === "Choose Country" ? true : filter(country, element.country);
    if (isCountry) {
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function checkContinent(data) {
  var continent = [];
  $.each($("input[name='continent']:checked"), function() {
    continent.push($(this).val());
  });
  console.log("CONTINENT", continent);
  var info = [];
  var i = 0;
  data.forEach(element => {
    continent.forEach(ele => {
      var isContinent = ele === "all" ? true : filter(ele, element.continent);
      if (isContinent) {
        // console.log("inside if");
        info[i++] = element;
      }
    });
  });
  return info;
}
function checkDate(data) {
  var date = $("input[name='date']:checked").val();
  console.log("DATE", date);
  var info = [];
  var i = 0;
  data.forEach(element => {
    var isDate = date === "all" ? true : filterDate(date, element.date);
    if (isDate) {
      // alert(isDate);
      console.log("DATE", element.date);
      info[i++] = element;
    }
  });
  // console.log("info", info);
  return info;
}
function filterDate(a, b) {
  var diff = monthDiff(b);
  switch (+a) {
    case 1: //less than one month
      if (diff < 1) {
        return true;
      } else return false;
      break;
    case 3: //less than 3 months
      if (diff < 3) {
        return true;
      } else return false;
      break;
    case 6: //less than 6 months
      if (diff < 6) {
        return true;
      } else return false;
      break;
    case 2: //less than one year
      if (diff < 12) {
        return true;
      } else return false;
      break;
  }
}

function monthDiff(dt1) {
  dt1 = new Date(dt1);
  var dt2 = new Date();
  var diffMonth = (dt2.getTime() - dt1.getTime()) / 1000;
  diffMonth /= 60 * 60 * 24 * 7 * 4;
  return Math.abs(Math.round(diffMonth));
}

function insertBadges(val) {
  var total_element = $(".element").length;
  $.each($("input[name='continent']:checked"), function() {
    var val1 = $(this).val();
    if (val == val1) {
      var lastid = $(".element:last").attr("id");
      var split_id = lastid.split("_");
      var nextindex = Number(split_id[1]) + 1;
      $(".element:last").after(
        "<div class='element' id='div_" + nextindex + "'></div>"
      );

      var span = $("<span>", {
        id: "remove_" + nextindex,
        val: val
      }).click(close_continent);
      span.addClass(["badge", "badge-pill", "badge-secondary", "p-2", "m-1"]);
      var icon = $("<i>");
      icon.addClass(["fa", "fa-close", "ml-1"]);
      span.text(val);
      span.append(icon);
      // div = "#" + div;

      $("#div_" + nextindex).append(span);
    }
  });
  // alert(total_element);
  // var lastid = $(".element:last").attr("id");
  // var split_id = lastid.split("_");
  // var nextindex = Number(split_id[1]) + 1;
  // $(".element:last").after(
  //   "<div class='element' id='div_" + nextindex + "'></div>"
  // );

  // var span = $("<span>", {
  //   id: "remove_" + nextindex,
  //   val: val
  // }).click(close_continent);
  // span.addClass(["badge", "badge-pill", "badge-secondary", "p-2", "m-1"]);
  // var icon = $("<i>");
  // icon.addClass(["fa", "fa-close", "ml-1"]);
  // span.text(val);
  // span.append(icon);
  // // div = "#" + div;

  // $("#div_" + nextindex).append(span);
}

function insertBadge(div, val) {
  var div_close = div + "_close";
  var span = $("<span>", {
    id: div_close
  }).click(close);
  span.addClass(["badge", "badge-pill", "badge-secondary", "p-2", "m-1"]);
  var icon = $("<i>");
  icon.addClass(["fa", "fa-close", "ml-1"]);
  span.text(val);
  span.append(icon);
  div = "#" + div;
  $(div).html(span);
}
function close() {
  // alert(this.id);
  var id = this.id.split("_");
  var radio_id = "#" + id[1] + "-all";
  // alert(id[1]);
  switch (id[1]) {
    case "price":
    case "date":
    case "report":
      $(radio_id).prop("checked", true);
      break;
    // case "category":
    default:
      document.getElementById(id[1]).selectedIndex = 0;
  }
  check();
  //alert("#" + this.id);
  $("#" + this.id).remove();
}
function close_continent() {
  var a = $("#" + this.id).text();
  var list = [];
  $.each($("input[name='continent']:checked"), function() {
    list.push($(this).val());
    var b = $(this).val();
    // alert(b);
    // alert(`working ${a}`);
    if (a == b) $(this).prop("checked", false);
  });
  $("#" + this.id).remove();
  // alert(list.length);
  if (list.length == 1) {
    $.each($("input[name='continent']"), function() {
      if ($(this).val() == "all") {
        // alert("...");
        $(this).prop("checked", true);
      }
    });
  }
  check();
}
function reset() {
  document.getElementById("category").selectedIndex = 0;
  document.getElementById("publisher").selectedIndex = 0;
  document.getElementById("country").selectedIndex = 0;
  document.getElementById("region").selectedIndex = 0;
  $.each($("input[name='continent']"), function() {
    if ($(this).val() == "all") {
      // alert("...");
      $(this).prop("checked", true);
    }
    $(this).prop("checked", false);
  });
  $("#div_price_close").remove();
  $("#div_date_close").remove();
  $("#div_category_close").remove();
  $("#div_publisher_close").remove();
  $("#div_report_close").remove();
  $("#div_region_close").remove();
  $("#div_country_close").remove();
  var total_element = $(".element").length;
  for (var i = 2; i <= total_element; i++) {
    $("#remove_" + i).remove();
  }
}
