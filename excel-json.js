const xlsx2json = require("xlsx-json-js");
// const path = require("path");
const xlsxPath = "admin/excel/insight.xlsx";
const jquery = require("jquery");

function excelToJson() {
    const nativeData = xlsx2json.parse(xlsxPath);
    //   console.log("inside excelToJson");
    // console.log(nativeData.length);
    // console.log(nativeData[0].data[0][0].length);
    for (var i = 0; i < nativeData.length; i++) {
        for (var j = 0; j < nativeData[i].data.length; j++) {
            var item = {
                name: nativeData[i].data[j][0],
                description: nativeData[i].data[j][1],
                date: nativeData[i].data[j][2],
                publisher: nativeData[i].data[j][3],
                price: string(nativeData[i].data[j][4]),
                category: nativeData[i].data[j][5],
                reportType: nativeData[i].data[j][6],
                continent: nativeData[i].data[j][7],
                region: nativeData[i].data[j][8],
                country: nativeData[i].data[j][9],
                noOfPages: nativeData[i].data[j][10],
                code: nativeData[i].data[j][11],
                src: nativeData[i].data[j][12],
                publishedType: nativeData[i].data[j][13],
                companiesCovered: nativeData[i].data[j][14]
            };
            console.log(item);
        }
    }
    //   var json = JSON.stringify(nativeData[0].data[0]);
    //   console.log(json);
}
// module.exports = excelToJson;
excelToJson();