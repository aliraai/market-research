const MongoClient = require("mongodb").MongoClient;

const dbConnectionUrl =
  "mongodb+srv://rashmi:qyUEWxq6waoLwK0g@mr-cz9bw.mongodb.net/test?retryWrites=true&w=majority";

function initialize(dbName, dbCollectionName) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(dbConnectionUrl, (err, dbInstance) => {
      if (err) {
        reject(err)
      } else {
        const dbObject = dbInstance.db(dbName);
        const dbCollection = dbObject.collection(dbCollectionName);
        console.log("MongoDB connection SUCCESSFULL");
        resolve(dbCollection);
      }
    });
  });
}

module.exports = {
  initialize
};